package com.squaredevops.spinnerimplementation;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class MainActivity2 extends AppCompatActivity {

    Spinner spinnerA, spinnerB, spinnerC;
    TextView textView;
    EditText editText;
    Button button, button2;

    // Spinner Drop down elements
    int[] elementsA = {100, 200, 300, 400};
    int[] elementsB = {700, 800, 900, 1000};
    String[] elementStringA = {"10", "20", "30", "40", "50"};
    String[] elementStringB = {"100", "200", "300", "400", "500"};
    String[] elementStringC = {"1", "2", "3", "4", "5"};
    String[] names = { "Farhan Sadik", "Nazmul Islam", "Mahmudul Haque", "Mahin"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        spinnerA = findViewById(R.id.spinner2);
        spinnerB = findViewById(R.id.spinner3);
        spinnerC = findViewById(R.id.spinner4);
        editText = findViewById(R.id.edittext1);
        textView = findViewById(R.id.printResult);
        button = findViewById(R.id.button2);
        button2 = findViewById(R.id.button3);

        // for spinner A
        ArrayAdapter arrayAdapter = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_spinner_item, elementStringA);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerA.setAdapter(arrayAdapter);
        spinnerA.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(getApplicationContext(), elementStringA[position], Toast.LENGTH_LONG).show();
                ((TextView) parent.getChildAt(0)).setTextColor(Color.BLUE);

                //String sel = spinnerA.setSelection(arrayAdapter.getPosition(elementStringA[position]));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // null
            }
        });

        // Spinner B
        ArrayAdapter arrayAdapter2 = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_spinner_item, elementStringB);
        arrayAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerB.setAdapter(arrayAdapter2);
        spinnerB.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(getApplicationContext(), elementStringB[position], Toast.LENGTH_LONG).show();
                ((TextView) parent.getChildAt(0)).setTextColor(Color.BLUE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // null
            }
        });

        // Spinner C
        ArrayAdapter arrayAdapter3 = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_spinner_item, elementStringC);
        arrayAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerC.setAdapter(arrayAdapter3);
        spinnerC.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(getApplicationContext(), elementStringC[position], Toast.LENGTH_LONG).show();
                ((TextView) parent.getChildAt(0)).setTextColor(Color.BLUE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // null
            }
        });

        // Button
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String valueA = spinnerA.getSelectedItem().toString();
                String valueB = spinnerB.getSelectedItem().toString();
                String valueC = spinnerC.getSelectedItem().toString();
                String valueD = editText.getText().toString();

                // converting string to integer
                int value1 = 0, value2 = 0, value3 = 0, value4 = 0;

                if (!"".equals(valueA)){
                    value1 = Integer.parseInt(valueA);
                }

                if (!"".equals(valueB)){
                    value2 = Integer.parseInt(valueB);
                }

                if (!"".equals(valueC)){
                    value3 = Integer.parseInt(valueC);
                }

                if (!"".equals(valueD)){
                    value4 = Integer.parseInt(valueD);
                }

                int sum = value1 + value2 + value3 + value4;

                // printing result
                textView.setText(
                        "A: " + valueA + "\n" +
                        "B: " + valueB + "\n" +
                        "C: " + valueC + "\n" +
                        "D: " + valueD + "\n" +
                        "SUM : " + sum);
            }
        });
        // end of button

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // intenting 2nd activity
                Intent i = new Intent(MainActivity2.this, MainActivity3.class);
                startActivity(i);
            }
        });
    }
}