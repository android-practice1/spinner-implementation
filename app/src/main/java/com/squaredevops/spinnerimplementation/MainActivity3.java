package com.squaredevops.spinnerimplementation;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.nio.file.attribute.GroupPrincipal;

public class MainActivity3 extends AppCompatActivity {

    EditText editText;
    Button button;
    Spinner spinner;
    TextView textView;

    String[] element = {"100", "200", "300", "400", "Custom"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        /*
            tested and updated Alpha 0.2.20
         */

        spinner = findViewById(R.id.spinner5);
        editText = findViewById(R.id.edittext2);
        button = findViewById(R.id.button4);
        textView = findViewById(R.id.printResult2);

        // disable edittext
        editText.setVisibility(View.GONE);

        // spinner
        ArrayAdapter arrayAdapter = new ArrayAdapter(getApplicationContext(),
                android.R.layout.simple_spinner_item, element);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0)).setTextColor(Color.BLUE);

                if (spinner.getSelectedItem().toString().equals(element[4])) {
                    editText.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // null
            }
        });

        String spinnerValue = spinner.getSelectedItem().toString();
        String edittextValue = editText.getText().toString();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (spinner.getSelectedItem().toString().equals(element[4])) {
                    textView.setText(editText.getText().toString());
                } else {
                    textView.setText(spinner.getSelectedItem().toString());
                }

                //textView.setText(spinner.getSelectedItem().toString());
            }
        });


    }
}